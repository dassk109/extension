<?php 
$sessionAllowWrite = true;
require_once('../interface/globals.php');
?>

<html>

<head>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

    <script src="script.js"></script>
    <title>
        Test Extension Page
    </title>

    <script>
        $(document).ready(function () {
            session = '<?php echo $_SESSION['token_main_php']; ?>';
            $('.innerIframe')
                .attr('src', 'http://127.0.0.1/openemr-6/interface/main/tabs/main.php?token_main='+session)
                .css({
                    'width': '100%',
                    'height': '100%'
                });
            $(window).on("load", function () {
                hidePanels();
            });
        });
    </script>
</head>

<body>
    <iframe class="innerIframe"></iframe>
</body>

</html>