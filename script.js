function hidePanels() {
    $(".innerIframe").contents().find("body").find("#mainBox").find("nav").hide();
    $(".innerIframe").contents().find("body").find("#mainBox").find("#attendantData").hide();
    $(".innerIframe").contents().find("body").find("#mainBox").find("#tabs_div").find(".tabControls").find("div.tabNotchosen.w-1").hide();
}